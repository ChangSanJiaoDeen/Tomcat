package method1;

public class Test1 {

	/*
	 * Classloader实现jsp的重新加载
	 * 
	 * Tomcat通过org.apache.jasper.servlet.JasperLoader实现了对jsp的加载， 下面做个测试：
	 *  1．新建一个web工程，并编写一个jsp页面，在jsp页面中输出该
	 * 页面的classloader,<%System.out.print(this.getClass().getClassLoader());%>.
	 * 2． 启动web服务器，打开jsp页面，我们可以看到后台输出，该jsp的classloader是JasperLoader的一个实例。 3．
	 * 修改jsp，保存并刷新jsp页面，再次查看后台输出，此classloader实例已经不是刚才那个了，也就是说tomcat
	 * 通过一个新的classloader再次装载了该jsp。 4． 其实，对于每个jsp页面tomcat都使用了一个独立的classloader
	 * 来装载，每次修改完jsp后，tomcat都将使用一个新的classloader来装载它
	 * 
	 */

}
